""" __init__.py - Wishbone couchdb input module """
from wishbone_output_http import HTTPOutClient


class OcdsapiInput(InputModule):

    def __init__(
            self,
            actor_config,
            api_url,
            native_events=False,
            destination="data",
            since=0,
            ):
        InputModule.__init__(self, actor_config)
        self.pool.createQueue("outbox")
    def preHook(self):
        pass

    def postHook(self):
        pass
    def produce(self):
        pass
