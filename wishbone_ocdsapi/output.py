import functools
from simplejson import loads
from urllib.parse import urljoin
from requests import Session
from requests.exceptions import RequestException
from wishbone.module import OutputModule
from wishbone.event import extractBulkItems


class APIOutput(OutputModule):
    """ Post releases to an api """
    def __init__(self,
                 config,
                 selection="data",
                 payload=None,
                 native_events=False,
                 parallel_streams=1,
                 headers={},
                 token='',
                 timeout=10,
                 api_root='http://localhost:6543'):
        OutputModule.__init__(self, config)
        self.pool.createQueue('inbox')
        self.registerConsumer(self.consume, 'inbox')
        self.session = Session()
        if token:
            self.session.auth = (token, '')
        self.post = functools.partial(
            self.session.post,
            urljoin(api_root, 'releases.json')
        )

    def consume(self, event):
        if event.isBulk():
            payload = {'releases': [
                e.get(self.kwargs.selection)
                for e in extractBulkItems(event)
            ]}
        else:
            payload = {'releases': [event.get(self.kwargs.selection)]}

        try:
            if payload['releases']:
                resp = self.post(
                    json=payload,
                    headers=self.kwargs.headers,
                    timeout=self.kwargs.timeout
                )
                try:
                    answer = resp.json()
                except:
                    answer = {"answer": resp.text}
                for id_, data in answer.items():
                    self.logging.info(f"{id_} -> {data}")
            else:
                self.logging.warn('Releases body is empty nothing to post')
        except RequestException as e:
            self.logging.error(f"Failed to post data to api")
