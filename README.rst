wishbonone-ocdsapi
==================

Input and Output wishbone module for ocds api

Example Usage
-----

.. code-block:: yaml

   modules:
     input:
       module: wishbone.module.input.couchdb
       arguments:
	 couchdb_url: "http://34.247.234.124/pb_production"
	 seqfile: var/seqfile
     tenders:
       module: wishbone.module.flow.jqfilter
       arguments:
	 conditions:
	 - name: Get only tenders
	   expression: '.doc_type == "Tender"'
	   queue: outbox

     pack:
       module: wishbone.module.process.pack
       arguments:
	 bucket_size: 1000

     process:
       module: wishbone.module.process.galleon
       arguments:
	 schema: schema.json
	 mapping: tenders.yml


     output_api:
       module: wishbone.module.output.ocdsapi
       arguments:
	 api_root: "http://localhost:6543"
	 token: ce5lah8oechu5ZohHei7ieTh7xiLahb6na3


   routingtable:
     - input.outbox -> tenders.inbox
     - tenders.outbox -> process.inbox
     - process.outbox -> output_api.inbox


Installation
------------

pip install wishbone_ocdsapi

Requirements
^^^^^^^^^^^^
See setup.py


Compatibility
-------------

Only python3 is currently supported

Licence
-------

Apache Licence 2.0

Authors
-------

`wishbonone-couchdb` was written by `yshalenyk <yshalenyk@quintagroup.com>`_.
