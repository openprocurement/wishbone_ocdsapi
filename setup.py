from setuptools import setup

VERSION = '0.1'
DESCRIPTION = """
Input and Output wishbone modules to work with OCDS API
"""
CLASSIFIERS = [
    'Development Status :: 2 - Pre-Alpha',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
]
INSTALL_REQUIRES = [
    'setuptools',
    'wishbone',
    'requests'
]
TEST_REQUIRES = [
    'pytest',
    'pytest-cov',
    'mock'
]
EXTRA = {
    "test": TEST_REQUIRES
}
ENTRY_POINTS = {
    'wishbone.module.input': [
        # 'ocdsapi = wishbone_ocdsapi:APIInput'
    ],
    'wishbone.module.output': [
        'ocdsapi = wishbone_ocdsapi:APIOutput'
    ],
}

setup(name='wishbone_ocdsapi',
      version=VERSION,
      description=DESCRIPTION,
      author='Quintagroup, Ltd.',
      author_email='info@quintagroup.com',
      license='Apache License 2.0',
      classifiers=CLASSIFIERS,
      include_package_data=True,
      packages=['wishbone_ocdsapi'],
      zip_safe=False,
      install_requires=INSTALL_REQUIRES,
      extras_require=EXTRA,
      entry_points=ENTRY_POINTS
      )
